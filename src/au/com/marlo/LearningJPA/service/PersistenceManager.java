package au.com.marlo.LearningJPA.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


public class PersistenceManager {
	
	private static EntityManagerFactory factory;
	
	public PersistenceManager(String PERSISTANCE_UNIT_NAME ) 
	{		
		this.factory = Persistence.createEntityManagerFactory(PERSISTANCE_UNIT_NAME);
	}
	
	public EntityManager getEntityManager () 
	{		
		EntityManager em = this.factory.createEntityManager();		
		return em;
	}		
}
