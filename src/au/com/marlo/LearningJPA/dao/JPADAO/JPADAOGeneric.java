package au.com.marlo.LearningJPA.dao.JPADAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import au.com.marlo.LearningJPA.service.PersistenceManager;

public class JPADAOGeneric {

	
	protected PersistenceManager pm;
	protected EntityManager em;
	protected EntityTransaction tx;
	
	public JPADAOGeneric () 
	{
	}
	
	
	public void add(Object T) 
	{	
		this.tx = this.em.getTransaction();
		this.tx.begin();
		this.em.persist(T);
		this.tx.commit();
	}
	
	public void delete(Object T) 
	{
		this.tx = this.em.getTransaction();
		this.tx.begin();
		Object ob = em.merge(T);
		em.remove(ob);
		this.tx.commit();
	}
	
	

	
	//not working
	public void update(Object T, int key) 
	{
	}

	public Object get(Class<?> c , int key) 
	{
		//this.tx = this.em.getTransaction();
		//this.tx.begin();
		Object t = this.em.find(c, key);
		return t;
	}
	
	public void close() 
	{
		this.pm.getEntityManager().close();
	}

}
