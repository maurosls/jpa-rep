package au.com.marlo.LearningJPA.dao.JPADAO;

import au.com.marlo.LearningJPA.dao.ProductDAO;
import au.com.marlo.LearningJPA.model.Product;
import au.com.marlo.LearningJPA.service.PersistenceManager;

public class JPAProductDAO extends JPADAOGeneric implements ProductDAO{

	
	public JPAProductDAO(PersistenceManager pm) 
	{
		this.pm = pm;
		this.em = this.pm.getEntityManager();
	}
	
	public void update(Product pd, int key) {
		this.tx = this.em.getTransaction();
		this.tx.begin();
		
		Product p = this.em.find(Product.class, key);
			
		p.setCategory(pd.getCategory());
		p.setDescription(pd.getDescription());
		p.setPrice(pd.getPrice());
			
		this.em.persist(p);
		this.tx.commit();
	}
	
	public void delete(int key) 
	{
		this.tx = this.em.getTransaction();
		this.tx.begin();
		Product product = (Product) this.get(Product.class, key);
		Product ob = em.merge(product);
		em.remove(ob);
		this.tx.commit();
	}
	
	
	
}
