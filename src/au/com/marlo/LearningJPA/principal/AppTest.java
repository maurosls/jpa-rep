package au.com.marlo.LearningJPA.principal;

import java.util.ArrayList;
import java.util.List;

import au.com.marlo.LearningJPA.dao.JPADAO.JPAClientDAO;
import au.com.marlo.LearningJPA.dao.JPADAO.JPAOrderDAO;
import au.com.marlo.LearningJPA.dao.JPADAO.JPAProductDAO;
import au.com.marlo.LearningJPA.model.Client;
import au.com.marlo.LearningJPA.model.Order;
import au.com.marlo.LearningJPA.model.Product;
import au.com.marlo.LearningJPA.service.PersistenceManager;


public class AppTest {

	public static void main(String[] args) {
		
		PersistenceManager pm = new PersistenceManager("test");
		
		System.out.println("...................................................");
		System.out.println("Iniciating JPA system...");
		System.out.println("...................................................");
		
		JPAProductDAO jpaProductDAO = new JPAProductDAO(pm);
		JPAClientDAO jpaClientDAO = new JPAClientDAO(pm);
		JPAOrderDAO jpaOrderDAO = new JPAOrderDAO(pm);


		Client client = new Client("Mauro",23,"Male",0);
		jpaClientDAO.add(client);

		
		Product product1 = new Product("JPA Book", 69.90,"Programing");
		Product product2 = new Product("Mouse wireless", 39.90, "Periferics");
		Product product3 = new Product("Nvidia gtx 1080 ti", 699.90, "Video graphics");
		
		jpaProductDAO.add(product1);
		jpaProductDAO.add(product2);
		jpaProductDAO.add(product3);
		
		List<Product> products = new ArrayList<Product>();
		
		products.add(product1);
		products.add(product2);
		products.add(product3);
		
		Order order = new Order();	
		order.setClient(client);
		order.setProducts(products);
		jpaOrderDAO.add(order);
		////////////////////////////////////////////////////
		
		//List<Product> productsOfAClient = jpaOrderDAO.getListOfProducts(jpaClientDAO.getKeyByName(client.getName()));
		
		List<Product> productsOfAClient = jpaClientDAO.getListOfProducts("Mauro",jpaOrderDAO);

				
		for (int i = 0; i < productsOfAClient.size(); i++) 
		{
			System.out.println(productsOfAClient.get(i).getCategory() + " | " + productsOfAClient.get(i).getDescription() +" | "+
		productsOfAClient.get(i).getCode());
		}
		
		
		/////////////////////////
		
		
		for (int i = 0; i < productsOfAClient.size(); i++) 
		{
			//System.out.println(productsOfAClient.get(i).getCode());
			//jpaOrderDAO.deleteWithProduct(productsOfAClient.get(i).getCode());
		}
		
		for (int i = 0; i < productsOfAClient.size(); i++) {
			//jpaProductDAO.delete(productsOfAClient.get(i).getCode());
		}
		
		//System.out.println("aqui");
		//REFATORAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1
		//jpaClientDAO.delete(jpaClientDAO.get(Client.class, jpaClientDAO.getKeyByName(client.getName())));
			
		jpaProductDAO.close();
		
	}
}