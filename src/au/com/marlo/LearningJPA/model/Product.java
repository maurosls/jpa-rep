package au.com.marlo.LearningJPA.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="PRODUCT")
public class Product {

	@Id
	@GeneratedValue	
	private int code;
	
	@Column(name="DESCRIPTION")
	private String description;
	
	@Column(name="PRICE")
	private double price;
	
	@Column(name="CATEGORY")
	private String category;
	
	@ManyToOne
	private Order order;
	
	
	public Product() 
	{		
	}
	
	public Product(String description, double price, String category) 
	{
		this.description = description;
		this.price = price;
		this.category = category;
	}
	
	//getters and setters
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	/*
	public Order getOrder() 
	{
		return this.order;
	}
	public void setOrder(Order order) 
	{
		this.order = order;
	}*/
}
