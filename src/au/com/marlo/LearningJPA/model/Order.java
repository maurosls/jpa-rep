package au.com.marlo.LearningJPA.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;



@Entity
@Table(name="\"ORDER\"")
public class Order {
	
	//ATRIBUTES
	@Id
	@GeneratedValue
	private int order_number;
	
	@OneToMany
	@JoinColumn
	@OrderBy
	private List<Product> products = new ArrayList<>();
	
	@OneToOne
	private Client client;

	
	//CONSTRUCTORS
	public Order()
	{	
	}
	
	public Order(ArrayList<Product> products, Client client)
	{	
		this.products= products;
		this.client = client;
	}
	
	public Order(Product products, Client client)
	{	
		this.products.add(products);
		this.client = client;
	}
	
	
	//GETTERS AND SETTERS
	public int getOrder_number() 
	{
		return order_number;
	}

	public void setOrder_number(int order_number) 
	{
		order_number = order_number;
	}

	
	
	public ArrayList<Product> getProducts() 
	{
		return (ArrayList<Product>) this.products;
	}

	public void setProducts(List<Product> product) 
	{
		this.products = products;
	}

	
	
	public Client getClient() 
	{
		return this.client;
	}

	public void setClient(Client client) 
	{
		this.client = client;
	}
	
}
