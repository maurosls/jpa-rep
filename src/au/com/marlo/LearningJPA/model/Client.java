package au.com.marlo.LearningJPA.model;

import javax.persistence.*;

@Entity
@Table(name="CLIENT")

public class Client {

	@Id
	@GeneratedValue	
	private int document_id;
	
	@Column(name="NAME")
	private String name;
	
	@Column(name="AGE")
	private int age;
	
	@Column(name="GENDER")
	private String gender;
	
	@Column(name="AVAIABLE_ACCOUNT_CREDIT")
	private int avaiable_account_credit;

	
	@OneToOne (mappedBy = "client")
	private Order order;
	
	
	public Client() 
	{
	}
	
	public Client(int key) 
	{
		this.document_id = key;
	}
	
	
	public Client(String name, int age, String gender, int avaiable_account_credit) 
	{
		this.name = name;
		this.age = age;
		this.gender = gender;
		this.avaiable_account_credit = avaiable_account_credit;
	}
	
	
	//GETTERS AND SETTERS
	public int getDocument_id() {
		return document_id;
	}

	public void setDocument_id(int document_id) {
		this.document_id = document_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAvaiable_account_credit() {
		return avaiable_account_credit;
	}

	public void setAvaiable_account_credit(int avaiable_account_credit) {
		this.avaiable_account_credit = avaiable_account_credit;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}
	
	
	
	
	
}
